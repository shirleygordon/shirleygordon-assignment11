#include "threads.h"



int main()
{
	std::ofstream file;
	
	call_I_Love_Threads();
	
	cout << endl << "Test adding primes to vector:" << endl;
	cout << "0-1000" << endl;
	std::vector<int> primes3 = callGetPrimes(0, 1000);
	//printVector(primes3);
	
	cout << endl << "0-100000" << endl;
	std::vector<int> primes4 = callGetPrimes(0, 100000);
	//printVector(primes4);
	
	cout << endl << "0-1000000" << endl;
	std::vector<int> primes5 = callGetPrimes(0, 1000000);
	//printVector(primes5);

	cout << endl << "Test writing primes to file:" << endl;
	cout << "0-1000" << endl;
	callWritePrimesMultipleThreads(0, 1000, "primes1.txt", 2);

	cout << endl << "0-100000" << endl;
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 5);

	cout << endl << "0-1000000" << endl;
	callWritePrimesMultipleThreads(0, 1000000, "primes3.txt", 10);
	
	system("pause");
	return 0;
}